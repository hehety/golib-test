package main

import (
    "github.com/urfave/cli"
    "os"
    "fmt"
)

/*func main() {
    f1 := &golibs.FlagEntity {
        Name: "p",
        Default: 1022,
        Usage: "specify port number\n-p <port number>",
        Type: reflect.Int,
    }
    ls := make([]*golibs.FlagEntity, 1)
    ls[0] = f1
    bs, _ := json.Marshal(f1)
    fmt.Println(string(bs))
    golibs.RegisterFlags(ls)
    bs, _ = json.Marshal(f1)
    fmt.Println(string(bs))
}
*/

func main() {

    var configLocation string
    var uploadLocation string
    app := cli.NewApp()
    app.HideHelp = true
    app.HideVersion = true

    app.Name = "go-libs test program"
    app.Author = "hehety"

    // flag选项
    app.Flags = []cli.Flag{
        cli.StringFlag{
            Name:        "config, c",
            Usage:       "Load configuration from `FILE`",
            Destination: &configLocation,
        },
    }
    // 子命令
    app.Commands = []cli.Command{
        {
            Name:    "upload",
            Aliases: []string{"u"},
            Usage:   "upload local file",
            Action: func(c *cli.Context) error {
                return nil
            },
            // 子命令的参数
            Flags: []cli.Flag{
                cli.StringFlag{
                    Name:        "file, f",
                    Usage:       "file path to be upload",
                    Destination: &uploadLocation,
                },
            },
        },
    }


    // 没有子命令时的默认行为
    app.Action = func(c *cli.Context) error {
        fmt.Println("welcome to test!")
        return nil
    }

    // 帮助文件模板
    cli.AppHelpTemplate = `{{if .VisibleFlags}}OPTIONS:
    {{range .VisibleFlags}}{{.}}{{end}}{{end}}{{if .Commands}}

COMMANDS:
    {{range .Commands}}{{if not .HideHelp}}   {{join .Names ", "}}{{ "\t"}}{{.Usage}}{{ "\n" }}{{end}}{{end}}{{end}}`

    err := app.Run(os.Args)
    if err != nil {

    } else {
        fmt.Println("config file is ", configLocation)
        fmt.Println("upload file is ", uploadLocation)
    }

}
